<nav id="mainnav-container">
    <div id="mainnav">

        <!--Menu-->
        <!--================================-->
        <div id="mainnav-menu-wrap">
            <div class="nano">
                <div class="nano-content">

                    <ul id="mainnav-menu" class="list-group">
                        <!--Menu list item-->
                        <li class="">
                            <a href="{{ route('devices.index') }}">
                                <i class="demo-psi-home icon-lg icon-fw"></i>
                                <span class="menu-title">
                                    <strong>Device Manager</strong>
                                </span>
                            </a>
                        </li>

                    </ul>

                </div>
            </div>
        </div>
        <!--================================-->
        <!--End menu-->

    </div>
</nav>
