@extends('layouts.layout')

@section('page-title')
    <div id="page-title">
        <h1 class="page-header text-overflow">Device Manager</h1>
    </div>
@endsection

@section('page-content')

    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Search</h3>
        </div>
        <form action="{{ route('devices.search') }}" method="GET">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Imei</label>
                            <input type="text" class="form-control" name="imei"
                                   value="{{ isset($search_param) ? $search_param['imei'] : '' }}">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Platform</label>
                            <input type="text" class="form-control" name="platform"
                                   value="{{ isset($search_param) ? $search_param['platform'] : '' }}"/>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Status</label>
                            <select class="form-control" name="status">
                                @foreach(\App\Enums\Status::getKeys() as $status)
                                    <option
                                        value="{{ $status }}" {{ \App\Enums\Status::getValue($status) == \App\Enums\Status::getValue($search_param['status']) ? 'selected' : '' }}>
                                        {{  \App\Enums\Status::getValue($status) }}
                                    </option>device
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 text-right">
                    <a href="{{ route('devices.index') }}" class="btn btn-default"><i
                            class="glyphicon glyphicon-refresh"></i> Clear</a>
                    <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> Search
                    </button>
                </div>
            </div>
        </form>
    </div>

    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Device List</h3>
        </div>

        <div class="panel-body">
            <div class="pad-btm form-inline">
                <div class="row">
                    <div class="col-sm-12 table-toolbar-right">
                        <button class="btn btn-primary" onclick="addDevice()"><i class="glyphicon glyphicon-plus"></i>
                            Add
                        </button>
                        <button id="bulk-active" type="button" class="btn btn-success" onclick="bulkActive()">
                            <i class="glyphicon glyphicon-ok-circle"></i> Active
                        </button>
                        <button id="buok-deactive" type="button" class="btn btn-warning" onclick="bulkDeactive()">
                            <i class="glyphicon glyphicon-ban-circle"></i>
                            Deactive
                        </button>
                        <button id="buld-delete" type="button" class="btn btn-danger" onclick="bulkDelete()">
                            <i class="glyphicon glyphicon-trash"></i> Delete
                        </button>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered" id="tblDevice">
                    <thead>
                    <tr>
                        <th class="text-center" id="thCheckAll">
                            <input id="checkAll" class="magic-checkbox" type="checkbox">
                            <label for="checkAll"></label>
                        </th>
                        <th>Imei</th>
                        <th>Platform</th>
                        <th>Token</th>
                        <th class="text-center">Statut</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @if (count($devices) > 0)
                        @foreach($devices as $device)
                            <tr>
                                <td class="text-center">
                                    <input id="{{ 'check_' . $device->id }}" class="magic-checkbox"
                                           type="checkbox" {{ $device->is_deleted ? 'disabled' : ''}}>
                                    <label for="{{ 'check_' . $device->id }}"></label>
                                    <input type="hidden" class="device_id" value="{{ $device->id }}"/>
                                </td>
                                <td>{{ $device->imei }}</td>
                                <td>{{ $device->platform }}</td>
                                <td>{{\Illuminate\Support\Str::limit($device->token, 60)}}</td>
                                <td class="text-center">
                                    @if($device->is_deleted)
                                        <div class="label label-table label-danger my-2">Deleted</div>
                                    @else
                                        <div
                                            class="label label-table  {{ $device->is_actived ? "label-success" : "label-warning"}}">
                                            {{ $device->is_actived ? 'Activated' : " In Activated " }}
                                        </div>
                                    @endif
                                </td>
                                <td class="text-center"
                                    style="max-width:100%; white-space: nowrap;">
                                    <a href="{{ route('devices.show', $device->id) }}" class="btn btn-success btn-icon">
                                        <i class="glyphicon glyphicon-eye-open"></i>
                                    </a>
                                    @if(!$device->is_deleted)
                                        <a href="{{ route('devices.edit', $device->id) }}"
                                           class="btn btn-warning btn-icon"><i class="glyphicon glyphicon-edit"></i>
                                        </a>

                                        <form action="{{ route('devices.destroy', $device->id) }}" method="POST">
                                            @csrf
                                            <button type="submit" class="btn btn-danger btn-icon mar-top"><i
                                                    class="glyphicon glyphicon-trash"></i>
                                            </button>
                                        </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach

                    @else
                        <tr>
                            No data found
                        </tr>
                    @endif
                    </tbody>
                </table>
                <div class="row">
                    <div class="form-group col-12 text-center">
                        {{ $devices->appends($search_param)->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            initCheckAllEvent();
        });

        function addDevice() {
            window.location = '{{ url(route('devices.create')) }}';
        }

        function bulkActive() {
            var ids = getSelectedDevices();
            if (ids.length > 0) {
                initComfirmationBox('Are you sure to active selected devices?', function () {
                    $.ajax({
                        url: "{{ route('devices.bulk-active') }}",
                        method: "PUT",
                        data: {
                            ids: ids,
                            _token: '{{csrf_token()}}'
                        },
                        success: function (data) {
                            location.reload();
                        }
                    })
                })
            } else {
                bootbox.alert("You must select at least 1 device!");
            }
        }

        function bulkDeactive() {
            var ids = getSelectedDevices();
            if (ids.length > 0) {
                initComfirmationBox('Are you sure to de-active selected devices?', function () {
                    $.ajax({
                        url: "{{ route('devices.bulk-deactive') }}",
                        method: "PUT",
                        data: {
                            ids: ids,
                            _token: '{{csrf_token()}}'
                        },
                        success: function (data) {
                            location.reload();
                        }
                    })
                })
            } else {
                bootbox.alert("You must select at least 1 device!");
            }
        }

        function bulkDelete() {
            var ids = getSelectedDevices();
            if (ids.length > 0) {
                initComfirmationBox('Are you sure to delete selected devices?', function () {
                    $.ajax({
                        url: "{{ route('devices.bulk-delete') }}",
                        method: "PUT",
                        data: {
                            ids: ids,
                            _token: '{{csrf_token()}}'
                        },
                        success: function (data) {
                            location.reload();
                        }
                    })
                })
            } else {
                bootbox.alert("You must select at least 1 device!");
            }
        }


        function getSelectedDevices() {
            return $("#tblDevice tbody").find("input[type=checkbox]:checked").map(function () {
                return $(this).siblings('input.device_id').val();
            }).get().filter(function (v) {
                return v !== ''
            });
        }

        function initCheckAllEvent() {
            $('#checkAll').click(function () {
                var isCheckAllChecked = $(this).prop("checked");
                var rows = $('#tblDevice tr:has(td)').find('input[type="checkbox"]');
                for (var x = 0; x < rows.length; x++) {
                    if (!$(rows[x]).is(':disabled')) {
                        $(rows[x]).prop('checked', isCheckAllChecked);
                    }
                }
            });
        }

        function initComfirmationBox(message, callback) {
            bootbox.confirm(message, function (result) {
                if (result) {
                    callback()
                } else {
                    $.niftyNoty({
                        type: 'danger',
                        icon: 'pli-cross icon-2x',
                        message: 'Cancelled',
                        container: 'floating',
                        timer: 3000
                    });
                }
                ;

            });
        }
    </script>
@endsection
