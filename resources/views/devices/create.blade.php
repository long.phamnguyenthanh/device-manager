@extends('layouts.layout')

@section('page-title')
    <div id="page-title">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page"><a class="page-header text-overflow" href="#">Device</a>
                </li>
            </ol>
        </nav>
    </div>
@endsection

@section('page-content')
    <div class="panel">
        <div class="panel-heading">
            @if( $mode === \App\Enums\Mode::VIEW)
                <h3 class="panel-title">Detail</h3>
            @elseif($mode === \App\Enums\Mode::CREATE)
                <h3 class="panel-title">Create</h3>
            @else
                <h3 class="panel-title">Update</h3>
            @endif
        </div>

        @if($errors->any())

            <div class="alert alert-danger">
                @foreach($errors->all() as $error)
                {{ $error }} </br>
                @endforeach
            </div>

        @endif

        @if(\App\Enums\Mode::CREATE == $mode)
            <form id="input-form" action="{{ route('devices.store') }}" method="POST">
                @csrf
                @include('devices.form-create')
            </form>
        @else
            <form id="input-form" action="{{ route('devices.update', $device['id']) }}" method="POST">
                @csrf
                @method('PUT')
                @include('devices.form-create')
            </form>
        @endif
    </div>
@endsection

@section('script')
    var switcheryElement;

    <script>
        $(document).ready(function () {
            initSwitchery();
            checkFormEditable({!!$mode === \App\Enums\Mode::VIEW !!});
            handleResetClick();
        });

        function initSwitchery() {
            var elem = document.querySelector('.js-switch');
            switcheryElement = new Switchery(elem);
        }

        function disableSwitchery() {
            switcheryElement.disable();
        }

        function checkFormEditable(isViewMode) {
            if (isViewMode) {
                disableSwitchery()
                var inputs = document.getElementsByTagName('input');
                for (var x = 0; x < inputs.length; x++) {
                    inputs[x].disabled = 'disabled';
                }
                var textAreas = document.getElementsByTagName('textarea');
                for (var x = 0; x < textAreas.length; x++) {
                    textAreas[x].disabled = 'disabled';
                }
            }
        };

        function handleResetClick() {
            $('#reset').on('click', function () {
                location.reload();
            });
        }
    </script>
@endsection
