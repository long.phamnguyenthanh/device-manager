<div class="panel-body">
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label text-bold">Imei</label>
                <input name="imei" id="imei" type="text" class="form-control" value="{{ $device['imei'] }}">
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label text-bold">Platform</label>
                <input name="platform" id="platform" type="text" class="form-control"
                       value="{{ $device['platform'] }}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label text-bold">Token</label>
                <textarea name="token" id="token" rows="4"
                          class="form-control">{{ $device['token'] }}</textarea>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label text-bold">Is active</label> <br>
                <!--Switchery : Checked-->
                <!--===================================================-->
                <input name="is_actived" id="is_actived" type="checkbox"
                       class="form-control js-switch" {{ $device['is_actived'] == 1 ? 'checked' : '' }}/>
                <!--===================================================-->
            </div>
        </div>
    </div>
</div>

<div class="panel-footer text-right">
    <a class="btn btn-warning" href="{{ route('devices.index') }}"><i
            class="glyphicon glyphicon-chevron-left"></i> Back</a>
    @if( $mode !== \App\Enums\Mode::VIEW)
        <button id="reset" type="button" class="btn btn-default"><i
                class="glyphicon glyphicon-refresh"></i> Reset
        </button>
        <button type="submit" class="btn btn-primary"><i
                class="glyphicon glyphicon-floppy-saved"></i> {{ \App\Enums\Mode::CREATE == $mode ? 'Create' : 'Save' }}
        </button>
    @endif
</div>
