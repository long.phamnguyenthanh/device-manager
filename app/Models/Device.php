<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{

    protected $fillable = [
        'employee_id',
        'imei',
        'platform',
        'token',
        'is_actived'
    ];
}
