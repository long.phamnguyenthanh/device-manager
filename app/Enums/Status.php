<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class Status extends Enum
{
    const ALL = 'All';
    const ACTIVED = 'Actived';
    const IN_ACTIVED = 'In Actived';
    const DELETED = 'Deleted';
}
