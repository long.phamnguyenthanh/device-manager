<?php


namespace App\Http\Services;


use App\Device;
use App\Http\Repositories\DevicesRepository;

class DevicesService
{
    /**
     * @var DevicesRepository
     */
    protected $deviceRepository;

    public function __construct(DevicesRepository $devicesRepository)
    {
        $this->deviceRepository = $devicesRepository;
    }

    public function getAll()
    {
        return $this->deviceRepository->getAll();
    }

    public function search($search_data)
    {
        return $this->deviceRepository->search($search_data);
    }

    public function store(Device $device)
    {
        $this->deviceRepository->store($device);
    }

    public function active($ids)
    {
        $this->deviceRepository->active($ids);
    }

    public function deactive($ids)
    {
        $this->deviceRepository->deactive($ids);
    }

    public function delete($ids)
    {
        $this->deviceRepository->delete($ids);
    }
}
