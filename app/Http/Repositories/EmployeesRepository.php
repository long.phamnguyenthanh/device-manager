<?php


namespace App\Http\Repositories;


use App\Employee;

class EmployeesRepository
{
    public function getActiveEmployees()
    {
        return Employee::all();
    }

}
