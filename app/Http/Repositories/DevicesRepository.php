<?php


namespace App\Http\Repositories;


use App\Device;
use App\Enums\Status;

class DevicesRepository
{

    public function getAll()
    {
        return Device::orderBy('id', 'ASC')->paginate(10);
    }

    public function search($search_param)
    {
        $statutQuery = '';
        if (Status::getKey(Status::ACTIVED) == $search_param['status']) {
            $statutQuery = 'AND is_actived = 1 AND is_deleted = 0';
        } else if (Status::getKey(Status::IN_ACTIVED) == $search_param['status']) {
            $statutQuery = 'AND is_actived = 0 AND is_deleted = 0';
        } else if (Status::getKey(Status::DELETED) == $search_param['status']) {
            $statutQuery = 'AND is_deleted = 1';
        }

        return Device::whereRaw(
            'UPPER(imei) like "%' . strtoupper($search_param['imei']) . '%" AND ' .
            'UPPER(platform) like "%' . strtoupper($search_param['platform']) . '%" ' .
            $statutQuery
        )->orderBy('id', 'ASC')->paginate(10);
    }

    public function deactive($ids)
    {
        Device::whereIn('id', $ids)->update(['is_actived' => 0]);
    }

    public function active($ids)
    {
        Device::whereIn('id', $ids)->update(['is_actived' => 1]);
    }

    public function delete($ids)
    {
        Device::whereIn('id', $ids)->update(['is_deleted' => 1]);
    }

    public function store(Device $device)
    {
        $device->save();
    }

}
