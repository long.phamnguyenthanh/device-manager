<?php

namespace App\Http\Controllers;

use App\Device;
use App\Enums\Mode;
use App\Enums\Status;
use App\Http\Requests\DeviceCreateRequest;
use App\Http\Requests\DeviceUpdateRequest;
use App\Http\Services\DevicesService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use PHPUnit\Exception;

class DevicesController extends Controller
{
    protected $devicesService;

    protected $STATUS_SUCCESS = 'success';
    protected $STATUS_FAIL = 'fail';

    protected $ROUTE_INDEX = 'devices.index';
    protected $ROUTE_CREATE = 'devices.create';

    protected $ATTR_DEVICE = 'device';
    protected $ATTR_MODE = 'mode';

    public function __construct(DevicesService $devicesService)
    {
        $this->devicesService = $devicesService;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        $responseData = [
            $this->ATTR_DEVICE => null,
            $this->ATTR_MODE => Mode::CREATE
        ];

        return view($this->ROUTE_CREATE)->with($responseData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param DeviceCreateRequest $request
     * @return RedirectResponse|Redirector
     */
    public function store(DeviceCreateRequest $request)
    {
        $device = Device::create([
            'imei' => $request->imei,
            'token' => $request->token,
            'platform' => $request->platform,
            'is_actived' => $request->is_actived == null ? 0 : 1,
            'employee_id' => Auth::user()->id
        ]);
        try {
            $this->devicesService->store($device);
            session()->flash($this->STATUS_SUCCESS, 'Device created successful!');
        } catch (Exception $e) {
            session()->flash($this->STATUS_FAIL, 'Fail to create device. Error: ' . $e);
        }
        return redirect(route($this->ROUTE_INDEX));
    }

    /**
     * Display the specified resource.
     *
     * @param Device $device
     * @return Factory|View
     */
    public function show(Device $device)
    {
        $responseData = [
            $this->ATTR_DEVICE => $device,
            $this->ATTR_MODE => Mode::VIEW
        ];
        return view($this->ROUTE_CREATE)->with($responseData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Device $device
     * @return Factory|View
     */
    public function edit(Device $device)
    {
        if ($device->is_deleted == 1) {
            session()->flash($this->STATUS_FAIL, 'Cannot edit deleted device!');
            return redirect(route($this->ROUTE_INDEX));
        }

        $responseData = [
            $this->ATTR_DEVICE => $device,
            $this->ATTR_MODE => Mode::EDIT
        ];
        return view($this->ROUTE_CREATE)->with($responseData);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $search_param = [
            'imei' => '',
            'platform' => '',
            'status' => Status::getKey(Status::ALL)
        ];

        return view($this->ROUTE_INDEX)->with([
                'devices' => $this->devicesService->getAll(),
                'search_param' => $search_param]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param DeviceUpdateRequest $request
     * @param Device $device
     * @return RedirectResponse|Redirector
     */
    public function update(DeviceUpdateRequest $request, Device $device)
    {
        if ($device->is_deleted == 1) {
            session()->flash($this->STATUS_FAIL, 'Cannot update deleted device!');
            return redirect(route($this->ROUTE_INDEX));
        }

        try {
            $device->imei = $request->imei;
            $device->platform = $request->platform;
            $device->token = $request->token;
            $device->is_actived = $request->is_actived == null ? 0 : 1;
            $device->employee_id = Auth::user()->id;

            $this->devicesService->store($device);
            session()->flash($this->STATUS_SUCCESS, 'Device updated successful!');
        } catch (Exception $e) {
            session()->flash($this->STATUS_FAIL, 'Fail to update device. Error: ' . $e);
        }
        return redirect(route($this->ROUTE_INDEX));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Device $device
     * @return RedirectResponse|Redirector
     */
    public function delete(Device $device)
    {
        if ($device->is_deleted == 1) {
            session()->flash($this->STATUS_FAIL, 'Cannot delete deleted device!');
            return redirect(route($this->ROUTE_INDEX));
        }

        try {
            $device->is_deleted = 1;
            $this->devicesService->store($device);
            session()->flash($this->STATUS_SUCCESS, 'Device deleted successful!');
        } catch (Exception $e) {
            session()->flash($this->STATUS_FAIL, 'Fail to delete device. Error: ' . $e);
        }
        return redirect(route($this->ROUTE_INDEX));
    }

    public function search(Request $request)
    {
        $search_param = [
            'imei' => $request->imei,
            'platform' => $request->platform,
            'status' => $request->status
        ];

        $result = $this->devicesService->search($search_param);
        return view($this->ROUTE_INDEX)->with([
            'devices' => $result,
            'search_param' => $search_param
        ]);
    }

    public function bulkActive(Request $request)
    {
        try {
            $this->devicesService->active($request->input('ids'));
            session()->flash($this->STATUS_SUCCESS, 'Devices active successful!');
        } catch (Exception $e) {
            session()->flash($this->STATUS_FAIL, 'Fail to active devices. Error: ' . $e);
        }
    }

    public function bulkDeactive(Request $request)
    {
        try {
            $this->devicesService->deactive($request->ids);
            session()->flash($this->STATUS_SUCCESS, 'Devices deactive successful!');
        } catch (Exception $e) {
            session()->flash($this->STATUS_FAIL, 'Fail to deactive devices. Error: ' . $e);
        }
    }

    public function bulkDelete(Request $request)
    {
        try {
            $this->devicesService->delete($request->ids);
            session()->flash($this->STATUS_SUCCESS, 'Devices delete successful!');
        } catch (Exception $e) {
            session()->flash($this->STATUS_FAIL, 'Fail to delete devices. Error: ' . $e);
        }
    }

}
