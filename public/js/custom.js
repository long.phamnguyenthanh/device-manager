$(document).ready(function () {
    $(".btn_search_level").click(function () {
        $(this).parent().find(".search_level").slideToggle();
        $(this).find(".show_search, .hide_search").toggle();
        // $(".hide_search").show();
    });
//date picker==
    $(".DatePicker").datepicker();
//    ====
    $(".box .fa-pencil-alt").click(function () {
        $(this).hide();
        $(this).closest(".box").find(".body_box").hide();
        $(this).closest(".box").find(".edit_address").show();
    });

    $(".box .btn-save").click(function () {
        $(this).closest(".box").find(".fa-pencil-alt").show();
        $(this).closest(".box").find(".body_box").show();
        $(this).closest(".box").find(".edit_address").hide();
    });
    $(".box-address i.fa-pencil").click(function () {
        $(this).hide();
        $(this).parent().parent().find(".body_box").hide();
        $(this).parent().parent().find(".edit_address").show();
    });

    $(".box-address .btn-save").click(function () {
        $(this).parent().parent().find(".body_box").show();
        $(this).parent().parent().find(".edit_address").hide();
        $(this).parent().parent().find(".fa-pencil").show();
    });
// check all
    $('#check-all').checkAll({
        target: '',
        reverse: true / false,
        checked: null / true / false,
        sync: true / false
    });

    function CheckForm() {
        var checked = false;
        var elements = document.getElementsByName("choose");
        for (var i = 0; i < elements.length; i++) {
            if (elements[i].checked) {
                checked = true;
            }
        }
        if (!checked) {
            $(".switch_bar").slideUp();
        }
        if (checked) {
            $(".switch_bar").slideDown();
        }
        return checked;
    }

    CheckForm();


    $('input[type="checkbox"].checkbox_check').click(function () {
        CheckForm();
    });

});
