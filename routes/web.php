<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', function () {
    return view('auth.login');
})->name('');
Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('devices', 'DevicesController');
    Route::post('/devices/{device}', 'DevicesController@delete')->name('devices.delete');
    Route::get('/search', 'DevicesController@search')->name('devices.search');
    Route::put('/bulk-active', 'DevicesController@bulkActive')->name('devices.bulk-active');
    Route::put('/bulk-deactive', 'DevicesController@bulkDeactive')->name('devices.bulk-deactive');
    Route::put('/bulk-delete', 'DevicesController@bulkDelete')->name('devices.bulk-delete');
});
