# DEVICE MANAGER

Description: This project is about managing devices, includes these function:

    1. Authentication (Login/Logout)
    
    2. Search devices
    
    3. View detail
    
    4. Insert device
    
    5. Update device
    
    6. Delete/ Delete multi devices
    
    7. Active/ Active multi devices
    
    8. Inactive/ Inactive multi devices
    
    

1. Clone the repository

    git clone https://gitlab.com/long.phamnguyenthanh/device-manager.git

2. Start the local development server

    php artisan serve